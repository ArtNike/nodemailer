let app = require('express')();
let server = require('http').Server(app);
let io = require('socket.io')(server);
let total=0;
let delivered=0;
server.listen(8999);
let redis = require("redis");
let client = redis.createClient();
let startInterval = null;

io.sockets.on('connection', function (socket) {
    console.log('connected');
    socket.on('start', ()=>{
        total=0;
        delivered=0;
        client.hkeys("email", (err, res)=>{
            total = res.length;
        });
        startInterval = setInterval(function () {
            start();
        }, 15000);
    });

    socket.on('startProduction', ()=>{
        total=0;
        delivered=0;
        client.hkeys("production", (err, res)=>{
            total = res.length;
        });
        startInterval = setInterval(function () {
            startProduction();
        }, 15000);
    });
});



let updateInterval = setInterval(function () {
   update(Object.keys(io.sockets.connected));
}, 1000);

function start(){
    client.hkeys("email",  (err, res)=> {
        if(res.length !==0){
            client.hget("email", res[0], (err, value)=>{
               console.log(value);
               mail(value);
               deleteByKey(res[0]);
               delivered++;
            });
        }
        if(res.length === 0){
            if(startInterval !== null){
                clearInterval(startInterval);
                startInterval = null;
            }
            total=0;
            delivered=0;
        }
    });

}

function startProduction() {
    client.hkeys("production",  (err, res)=> {
        if(res.length !==0){
            client.hget("production", res[0], (err, value)=>{
                console.log(value);
                productionMail(value);
                deleteByKey(res[0], "production");
                delivered++;
            });
        }
        if(res.length === 0){
            if(startInterval !== null){
                clearInterval(startInterval);
                startInterval = null;
            }
            total=0;
            delivered=0;
        }
    });
}


function mail(res) {
    res = JSON.parse(res);
    var request = require('request');

    console.log(res);
    request.post(
        'http://localhost/send/mail',
        { json: {user_id: res.user_id, project_id: res.project_id} },
        function (error, response, body) {
            if (!error && response.statusCode == 200) {
                console.log('mail delivered');
            }
            else console.log(error);
        }
    );
}


function productionMail(res) {
    res = JSON.parse(res);
    var request = require('request');

    console.log(res);
    request.post(
        'http://localhost/send/mail/production',
        { json: {user_id: res.user_id, project_id: res.project_id} },
        function (error, response, body) {
            if (!error && response.statusCode == 200) {
                console.log('mail delivered');
            }
            else console.log(error);
        }
    );
}


function deleteByKey(key, hkey = "email") {
    client.hdel(hkey, key);
}

function update(connectedClients) {
    for(let i =0; i < connectedClients.length; i++){
        let msg = getUpdateMessage(total, delivered);
        io.sockets.connected[connectedClients[i]].emit('update', msg);
    }
}

function getUpdateMessage(total, delivered) {
    let msg = {};
    msg.status = 'error';
    if(total === 0){
        msg.status = 'pending';
    }
    else {
        msg.status = 'working';
        msg.total = total;
        msg.delivered = delivered;
    }
    return JSON.stringify(msg);
}
